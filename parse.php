<!DOCTYPE HTML>
<html>
<head>
    <meta charset="utf-8">
    <title>FaceMash</title>
</head>
<body>
<?php
    /**
     * Markov Chain - funny
     * phyton code: http://grantslatton.com/hngen/
     */
 
    mb_internal_encoding("UTF-8"); // set internal character encoding to UTF-8
	$titles = array();
    $markov_map = array();
	
	$json = $_POST['data'];
	
	$i = 0;
	foreach ($json as $obj) {
		$titles[] = trim($obj['message']);
		$i++;
	}
 
    $lookback = 2;
 
    // Generate map in the form word1 -> word2 -> occurences of word2 after word1
    foreach ($titles as $title) {
        $title2 = explode(" ", trim($title));
        $len = count($title2);
        if ($len > $lookback) {
            for ($i=0; $i < $len; $i++) {
                $previous_n_words = array();
 
                for ($j=max(0, $i-$lookback); $j < $i; $j++) {
                    $previous_n_words[] = $title2[$j];
                }
 
                $previous_n_words2 = implode(" ", $previous_n_words);
 
                if (!isset($markov_map[$previous_n_words2])) {
                    $markov_map[$previous_n_words2] = array();
                }
 
                $next_word = '';
 
                if ($i != $len) { // better "<" ??
                    $next_word = $title2[$i];
                }
 
                if (!isset($markov_map[$previous_n_words2][$next_word])) {
                    $markov_map[$previous_n_words2][$next_word] = 0;
                }
 
                $markov_map[$previous_n_words2][$next_word] += 1;
            }
        }
    }
 
    // Convert map to the word1 -> word2 -> probability of word2 after word1
    foreach ($markov_map as $word => $following) {
        $total = 0.0;
 
        foreach ($following as $value) {
            $total += $value;
        }
 
 
        foreach ($following as $key => $val) {
            $markov_map[$word][$key] /= ($total * 1.0);
        }
    }
 
 
    // Typical sampling from a categorical distribution
    function sample($items) {
        $next_word = '';
        $t = 0.0;
 
        foreach ($items as $k => $v) {
            $t += $v*1.0;
            if ($t > 0.0 && (mt_rand() / mt_getrandmax()) < $v/$t) { // random num based on Mersenne Twister instead of bad libc rand()
                $next_word = $k;
            }
        }
 
        return $next_word;
    }
	
	function getLinkified($text) {
		return $text."&nbsp;<a href='https://www.facebook.com/dialog/feed?
  app_id=1391880867727123
  &display=popup&caption=".urlencode($text)."
  &link=http://nachiketapte.com/facemash
  &redirect_uri=http://nachiketapte.com/facemash'>Share</a>"; // TODO
	}
 
 
    $sentences = array();
    while (count($sentences) < 10) {
        $sentence = array();
        $next_word = sample($markov_map['']);
 
        while ($next_word != '') {
            $sentence[] = $next_word;
            $previous_n_words = array();
 
            for ($i=max(0, count($sentence)-$lookback); $i < count($sentence); $i++) {
                $previous_n_words[] = $sentence[$i];
            }
 
            $previous_n_words2 = implode(" ", $previous_n_words);
 
            if (isset($markov_map[$previous_n_words2])) {
                $next_word = sample($markov_map[$previous_n_words2]);
            }
            else { // nothing, the end of a sentence !?
                $next_word = '';
            }
        }
 
        $sentence2 = implode(" ", $sentence);
 
        $flag = true;
        foreach ($titles as $title) { // Prune titles that are substrings of actual titles
            if (mb_strpos($title, $sentence2) !== FALSE) { // substring search
                $flag = false;
            }
        }
        if ($flag) {
            $sentences[] = $sentence2;
        }
    }
 
    echo "<ul>";
    foreach ($sentences as $sentence2) {
        echo "<li>".getLinkified($sentence2)."</li>";
    }
    echo "</ul>";

?>
</body>
</html>